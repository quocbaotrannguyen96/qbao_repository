﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRUD_EF_Final.Models;

namespace CRUD_EF_Final.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<User> obj;
            using (UserRepo db = new UserRepo())
            {
                obj = db.SelectAll();
            }
            return View(obj);
        }
        [HttpGet]
        public ActionResult AddUser()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddUser(User obj)
        {
            using(UserRepo db = new UserRepo())
            {
                db.Insert(obj);
                db.Save();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult EditUser()
        {
            return View();
            
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult EditUser(User obj)
        {
            using (UserRepo db = new UserRepo())
            {
                db.Update(obj);
                db.Save();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult DeleteUser()
        {
            return View();

        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult DeleteUser(string id)
        {
            using (UserRepo db = new UserRepo())
            {
                db.Delete(id);
                db.Save();
            }
            return RedirectToAction("Index");
        }
    }
}