﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRUD_EF_Final.Models
{
    public class UserRepo: IUser<User, string>, IDisposable
    {
        private UserContext db;
        public UserRepo()
        {
            this.db = new UserContext();
        }
        public void Delete( string id)
        {
            //User  = db.user.Where(x => x.UserName == id).SingleOrDefault();
            User user = db.user.Where(x => x.UserName == id).FirstOrDefault();
            db.user.Remove(user);
            
        }
        public void Dispose()
        {
            db.Dispose();
        }

        public void Insert(User obj)
        {
            db.user.Add(obj);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public List<User> SelectAll()
        {
            return db.user.ToList();
        }

        public User SelectByID(string id)
        {
            return db.user.Where(x => x.UserName == id).SingleOrDefault();
        }
        public void Update(User obj)
        {
            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
        }
    }
}