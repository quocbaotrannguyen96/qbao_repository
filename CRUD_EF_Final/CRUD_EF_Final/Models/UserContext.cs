﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace CRUD_EF_Final.Models
{
    public class UserContext : DbContext
    {
        public UserContext() : base()
        {

        }
        public DbSet<User> user { get; set; }
    }
}