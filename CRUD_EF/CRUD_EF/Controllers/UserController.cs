﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRUD_EF.Models;
namespace CRUD_EF.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            using (DBModels db = new DBModels())
            {
                return View(db.Users.ToList());
            }
        }

        // GET: User/Details/5
        public ActionResult Details(string id)
        {   using (DBModels db = new DBModels())
            {
                return View(db.Users.Where(x => x.UserName == id).FirstOrDefault()); ;
            }
            
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(User user)
        {
            try
            {
                using (DBModels db = new DBModels())
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                }
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(string id)
        {
            using (DBModels db = new DBModels())
            {
                return View(db.Users.Where(x => x.UserName == id).FirstOrDefault()); ;
            }
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, User user)
        {
            try
            {   
                using (DBModels db = new DBModels())
                {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                }
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(string id)
        {
            using (DBModels db = new DBModels())
            {
                return View(db.Users.Where(x => x.UserName == id).FirstOrDefault()); ;
            }
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(string id, User user)
        {
            try
            {
                // TODO: Add delete logic here
                using(DBModels db = new DBModels())
                {
                    user = db.Users.Where(x => x.UserName == id).FirstOrDefault();
                    db.Users.Remove(user);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
