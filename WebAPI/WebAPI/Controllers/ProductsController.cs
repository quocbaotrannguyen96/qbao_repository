﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;


namespace WebAPI.Controllers
{
    public class ProductsController : ApiController
    {
        Product[] products = new Product[]
        {
            new Product { Id = 1, Name ="a", Category = "A", Price = 100 },
            new Product { Id = 2, Name ="b", Category = "B", Price = 200 },
            new Product { Id = 3, Name ="c", Category = "C", Price = 300 },
            new Product { Id = 4, Name ="d", Category = "D", Price = 400 },
            new Product { Id = 5, Name ="e", Category = "E", Price = 500 },
            new Product { Id = 6, Name ="f", Category = "F", Price = 600 }
        };
        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }
        public IHttpActionResult GetProduct(int id)
        {
            var product = products.FirstOrDefault((p) => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
    }
}
